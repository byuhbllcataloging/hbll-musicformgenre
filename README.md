# hbll-musicformgenre

A repository of documentation developed by the Harold B. Lee Library at Brigham Young University to assist with running the [MusicFormGenre tool](http://files.library.northwestern.edu/public/MusicFormGenre) against a set of music MARC records.

## File Manifest

*Coming soon*

## Procedure

1. Install [MusicFormGenre](http://files.library.northwestern.edu/public/MusicFormGenre) on a PC running Windows
2. Extract MARC records from your ILS where the `Rec_Type` fixed field is either c, d, or j (see [LC MARC Leader](https://www.loc.gov/marc/bibliographic/bdleader.html) or [OCLC Type Fixed field](https://www.oclc.org/bibformats/en/fixedfield/type.html) documentation)
3. Pre-process records - Use a tool like [MarcEdit](http://marcedit.reeset.net) to manipulate the records - *Optional*: Remove local fields (i.e `583`, `59x`, `9xx`) if these files will be duplicated when loading back in to your ILS - *Optional*: Fix any records with problematic headings (e.g. `=650  \0$aViola and music$vScores and parts.`)
4. Run the pre-processed records through MusicFormGenre
    - Set the path to the input MARC file
    - Set the path to a folder for the output files
    - Set the parameters for how to handle 650, 655, and 382 fields
    - Click `Perform` to start processing records
5. Post-process records
    - Add action note in the `583` field based on the output category for each record (e.g. `NothingHappened`, `RecordsChanged.Message`, `RecordsChanged.OK`, `RecordsChanged.OK.DeleteReason`)
    - Example `583`: `=583  \\$amod$bMusicFormGenre-RecordsChanged.OK$c20190819$kgfr$5UPB`
    - Perform additional modifications to the headings to avoid data loss
    - Example: Flip identified headings in the `650` field to a `655` field where the heading doesn't generate useful genre information but has data in the heading that shouldn't be lost
6. Load the records back into your ILS
    - Work with your library's IT staff to load records if you aren't able to load them yourself

## Heading Analysis

Follow these steps to perform an analysis of headings in the `650` and `655` fields to help understand your MARC data in preparation for running [MusicFormGenre](http://files.library.northwestern.edu/public/MusicFormGenre) on a PC running Windows.

1. Download [MarcEdit](http://marcedit.reeset.net) to help generate data for the heading analysis
2. Extract MARC records from your ILS where the `Rec_Type` fixed field is either c, d, or j (see [LC MARC Leader](https://www.loc.gov/marc/bibliographic/bdleader.html) or [OCLC Type Fixed field](https://www.oclc.org/bibformats/en/fixedfield/type.html) documentation)
3. Export `650` and `655` heading data from the extracted MARC records into a tab delimited file
    - `MarcEdit` > `Tools` > `Export...` > `Export Tab Delimited Records`
    - Use settings file as a starting point for your analysis
4. Run Python script against the exported tab delimited file to generate a list of headings and their count
5. Run Python script against the heading count list file to generate dummy MARC records for each heading
6. Compile the dummy MARC records of each heading into a single `.mrc` file
7. Run the compiled dummy MARC records through MusicFormGenre
8. Generate csv files from the MusicFormGenre output files
9. Run Python script pointing to the location of the output files to generate the heading analysis spreadsheet
10. Analyze the headings and their output in the spreadsheet and distinguish headings that have no problems with headings that have problems that need addressing

## Updates to this repository

The contents of this repository and documentation is subject to change and will be updated. To recieve updates for this project follow [this respository](https://gitlab.com/byuhbllcataloging/hbll-musicformgenre) on [gitlab]( https://gitlab.com/users/sign_up). Alternatively, [enter your email](https://docs.google.com/forms/d/1Cg4RWptOkfvvJu-yNxIiFSuSpoC_HeH9BZspbY2wOZQ/edit) to be notified by email.
