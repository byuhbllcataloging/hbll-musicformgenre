#!/usr/bin/env python
import argparse
import csv
import logging
import re

from collections import Counter

logger = logging.getLogger(__name__)

FIELD_SUBJECT = '650'
FIELD_GENRE = '655'
FIELD_SOURCE = '655$2'
SUBFIELD_A = '$a'
SUBFIELD_2 = '$2'


def get_headings_and_sources(filepath):
    raw_headings = []
    raw_sources = []

    with open(filepath) as fin:
        reader = csv.DictReader(fin, dialect='excel-tab')
        for row in reader:
            subjects = row[FIELD_SUBJECT]
            genres = row[FIELD_GENRE]
            sources = row[FIELD_SOURCE]

            if subjects:
                raw_headings.extend(subjects.split(';'))

            if genres:
                raw_headings.extend(genres.split(';'))

            if sources:
                raw_sources.extend(sources.split(';'))

    return (raw_headings, raw_sources)


def parse_headings(headings):
    parsed_headings = []

    for heading in headings:
        split_heading = heading.split(SUBFIELD_A)
        if len(split_heading) == 2:
            parsed_headings.append(split_heading[1])
        elif len(split_heading) > 2:
            parsed_headings.extend(split_heading[1:])

    return parsed_headings


def process_headings(headings):
    processed_headings = []

    for heading in headings:
        processed_heading = None

        if SUBFIELD_2 in heading:
            processed_heading = heading.split(SUBFIELD_2)[0]
        else:
            processed_heading = heading

        processed_headings.append(
            '--'.join(re.split(r'\$.', processed_heading))
        )

    return processed_headings


def count_headings_and_sources(headings, sources):
    count_headings = Counter(headings)
    count_sources = Counter(sources)
    return (count_headings, count_sources)


def write_counts(count_headings, count_sources):
    with open('heading_extract_heading_count.csv', 'w') as fout:
        writer = csv.writer(fout, delimiter='\t')
        writer.writerow(('Heading', 'count'))
        writer.writerows(count_headings.most_common())

    with open('heading_extract_sources_count.csv', 'w') as fout:
        writer = csv.writer(fout, delimiter='\t')
        writer.writerow(('Source', 'count'))
        writer.writerows(count_sources.most_common())


def get_argument_parser():
    parser = argparse.ArgumentParser(
        prog='count_headings',
        description='A Python script to count unique catalog headings',
    )
    parser.add_argument(
        'filepath',
        help='The path to the source csv file with headings to count',
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action='store_true',
        default=False,
    )

    return parser


def main():
    # Setup command line arguments
    parser = get_argument_parser()

    # Parse command line arguments and config file
    args = parser.parse_args()

    # Setup logging
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    # Your code here
    headings, sources = get_headings_and_sources(args.filepath)
    parsed_headings = parse_headings(headings)
    processed_headings = process_headings(parsed_headings)
    count_headings, count_sources = count_headings_and_sources(
        processed_headings, sources
    )
    write_counts(count_headings, count_sources)


if __name__ == '__main__':
    main()
