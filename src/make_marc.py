#!/usr/bin/env python
import argparse
import csv
import logging

logger = logging.getLogger(__name__)

FILENAME = '{}.mrk'
FST = '(OCoLC)fst'


def get_headings(report):
    headings = []
    with open(report) as fin:
        reader = csv.DictReader(fin, dialect='excel-tab')
        for row in reader:
            data = row['Heading']
            headings.append(data)
    return headings


def get_template(template):
    data = None
    with open(template) as fin:
        data = fin.read()
    return data


def get_argument_parser():
    parser = argparse.ArgumentParser(
        prog='make_marc',
        description='A python package to make dummy marc files',
    )
    parser.add_argument('report', help='the input report file')
    parser.add_argument('template', help='the input template file')
    parser.add_argument('output', help='the output filepath')
    parser.add_argument('-v', '--verbose', action='store_true', default=False)

    return parser


def main():
    # Setup command line arguments
    parser = get_argument_parser()

    # Parse command line arguments
    args = parser.parse_args()

    # Setup logging
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    headings = get_headings(args.report)
    template = get_template(args.template)

    for index, heading in enumerate(headings):
        if FST not in heading:
            if '--' in heading:
                # TODO add $y for date span
                # TODO get actual subfields for heading data
                heading = heading.replace('--', '$v')

            data = template.format(index, heading + '\n')

            if args.output.endswith('/'):
                filepath = args.output + FILENAME.format(index)
            else:
                filepath = args.output + '/' + FILENAME.format(index)

            with open(filepath.format(index), 'w') as fout:
                fout.write(data)


if __name__ == '__main__':
    """
    `python make_marc.py reports/650_655_merged_headings.csv template.mrk reports/headings/`
    `python make_marc.py heading_extract_heading_count.csv template.mrk output/`
    """
    main()
