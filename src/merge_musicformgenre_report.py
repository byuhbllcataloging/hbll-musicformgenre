import csv

from unicodedata import normalize

REGEX_RECORD = r'((.\d\d\d.+\n){2,})'

DATE = '20200205.145553'
REPORTS = {
    'ok': 'output/logs/MusicFormGenre.{}.RecordsChanged.OK.{}',
    'message': 'output/logs/MusicFormGenre.{}.RecordsChanged.Message.{}',
    'nothing': 'output/logs/MusicFormGenre.{}.NothingHappened.{}',
}

SCORES = '\\7$aScores.$2lcgft'
PARTS = '\\7$aParts (Music)$2lcgft'
SCORES_PARTS = '{};{}'.format(SCORES, PARTS)

MERGED_HEADINGS_FIELDS = [
    'finished',
    'duplicate_count',
    'heading',
    'count',
    'no_problem',
    'flip',
    'no_382',
    'no_genre',
    'no_useful_genre',
    'scores',
    'parts',
    'language',
    'demographic',
    'misc_problems',
    'notes',
    '348',
    '370',
    '382',
    '655_1',
    '655_2',
    '655_3',
    '655_4',
    '655_5',
]

NO_650 = [
    '',
    '',
    113056,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
]


old_headings = {}

with open('650_report_heading_count - 650 + 655s updated counts.tsv') as fin:
    reader = csv.reader(fin, dialect='excel-tab')
    header = next(reader)
    print(header)
    for row in reader:
        old_headings[row[2]] = row[:2] + row[3:]

merged_headings = []
rows = []

for report, filepath in REPORTS.items():
    print(report)

    rows = None

    with open(filepath.format(DATE, 'csv')) as fin:
        reader = csv.reader(fin, dialect='excel-tab')
        header = next(reader)
        print(header)
        rows = list(reader)

    for row in rows:
        # 001
        record_id = row[0]
        # 348
        tag_348 = row[1]
        # 370
        tag_370 = row[2]
        # 382
        tag_382 = row[3]
        # 650
        tag_650 = row[4]
        # 655
        tag_655 = row[5]

        heading = tag_650.replace('\\0$a', '').replace('$v', '--')

        if '$y' in heading:
            heading = heading.replace('$y', '--')

        if '$2' in heading:
            heading = heading.replace('$2', '--')

        try:
            # match = old_headings[normalize('NFC', heading)]
            match = old_headings[heading]
            finished = match[0]
            duplicate_count = match[1]
            count = match[2]
            no_problem = match[3]
            flip = match[4]
            no_instrumentation = match[5]
            no_genre = match[6]
            no_useful_genre = match[7]
            scores = match[8]
            parts = match[9]
            language = match[10]
            demographic = match[11]
            misc_problems = match[12]
            notes = match[13]
        except Exception:
            print('{}|{}|{}'.format(record_id, tag_650, heading))
            continue
            # finished = ''
            # duplicate_count = ''
            # count = ''
            # no_problem = ''
            # flip = ''
            # no_instrumentation = ''
            # no_genre = ''
            # no_useful_genre = ''
            # scores = ''
            # parts = ''
            # language = ''
            # demographic = ''
            # misc_problems = ''
            # notes = ''

        if not tag_655:
            no_genre = 'x'

        if SCORES in tag_655:
            scores = 'x'

        if PARTS in tag_655:
            parts = 'x'

        if not tag_382:
            no_instrumentation = 'x'

        if tag_655 == SCORES_PARTS or tag_655 == SCORES or tag_655 == PARTS:
            no_useful_genre = 'x'

        merged_heading = [
            finished,
            duplicate_count,
            heading,
            # normalize('NFC', heading),
            count,
            no_problem,
            flip,
            no_instrumentation,
            no_genre,
            no_useful_genre,
            scores,
            parts,
            language,
            demographic,
            misc_problems,
            notes,
            tag_348,
            tag_370,
            tag_382,
        ]

        merged_heading.extend(tag_655.split(';'))

        merged_headings.append(merged_heading)

merged_headings.sort(key=lambda x: int(x[3]), reverse=True)

with open('650_655_merged_headings_musicformgenre_output.csv', 'w') as fout:
    writer = csv.writer(fout, delimiter='\t')
    writer.writerow(MERGED_HEADINGS_FIELDS)
    writer.writerow(NO_650)
    writer.writerows(merged_headings)
