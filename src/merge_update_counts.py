import csv

old_headings = {}
new_headings = {}
merged_headings = []
header = None

with open('650_report_heading_count - 650 + 655s merged.tsv') as fin:
    reader = csv.reader(fin, dialect='excel-tab')
    header = next(reader)
    print(header)
    for row in reader:
        old_headings[row[2]] = row[:2] + row[3:]


with open('heading_extract_heading_count.csv') as fin:
    reader = csv.reader(fin, dialect='excel-tab')
    print(next(reader))
    for row in reader:
        heading = row[0]
        new_headings[heading] = row[1]


for new_heading, count in new_headings.items():
    match = old_headings.get(new_heading)
    if match:
        match[2] = count
        match.insert(2, new_heading)
    else:
        match = [''] * 26
        match[2] = count
        match.insert(2, new_heading)
    merged_headings.append(match)


with open('650_report_heading_count_updated_counts.csv', 'w') as fout:
    writer = csv.writer(fout, delimiter='\t')
    writer.writerow(header)
    writer.writerows(merged_headings)
